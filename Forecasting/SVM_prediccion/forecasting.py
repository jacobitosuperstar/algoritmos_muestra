import numpy  as np
from sklearn.svm import SVR
# import matplotlib.pyplot as plt

x=np.linspace(1,100,100)
y_=x+np.sin(x)

Y=[]
for i in range(len(y_)-3):
    Y.append([y_[i],y_[i+1],y_[i+2],y_[i+3]])

Y=np.array(Y)

y=np.reshape(Y[:,3],(97,1))
print('y: ',y.shape)
X=Y[:,[0,1,2]]
print('X: ',X.shape)

x_test=np.linspace(100,150,50)
y_test=x_test+np.sin(x_test)

X_test_ls=[]
y_pred=[]

modelo=SVR(kernel='linear',gamma='scale', epsilon=0.001)
modelo.fit(X,y)

X_test_ls.append(np.array([97.37960774, 97.42661813, 98.00079317]))
X_test=np.array(X_test_ls)
y_pred.append(modelo.predict(X_test))

for i in range(0,len(y_test)-1):
    a=np.reshape(np.array(X_test_ls[i]),(3,1))
    b=np.reshape(np.array(y_pred[i]),(1,1))
    c=np.array([[a[1],a[2],b]])
    c=np.reshape(c,(1,3))
    X_test_ls.append(c)
    d=modelo.predict(c)
    y_pred.append(d)

y_test=np.reshape(np.array(y_test),(50,1))
y_pred=np.reshape(np.array(y_pred),(50,1))

cuadratic_error=np.zeros(shape=(len(y_test),1))

for i in range(len(y_test)):
    cuadratic_error[i]=((y_test[i]-y_pred[i])**2)/2

print('vector cuadratic_error: ',cuadratic_error)
print('promedio error cuadratico medio: ',np.average(cuadratic_error))

# plt.figure()
# plt.plot(np.reshape(x_test,(50,1)),y_test, color='black', label='datos_prueba')
# plt.plot(np.reshape(x_test, (50,1)),y_pred, color='green', label='datos_predecidos')
# plt.xlabel('X')
# plt.ylabel('Y')
# plt.title('support vector regression')
# plt.legend()
# plt.show()