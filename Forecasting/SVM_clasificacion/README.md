# SVM CLASIFICACIÓN #

## SE HACE UN DOCKER PARA CORRER LA APLICACIÓN ##

Se hace un dockerfile con alpine para correr la aplicación de svm.py en un container de tamaño mínimo.
Se usa el ambiente de miniconda debido a la flexibilidad que tiene para instalar y manejar paquetes, ya que no sólo se liminta a python, permite un manejo general de paquetes tanto en R, php, C++, entre otros.

Se paga con el tamaño de la imagen, ya que en promedio no baja de 1 giga, pero la facilidad de construcción y manejo valen la pena.

a través de la términal se accede a la carpeta en donde se clonó el repositorio y se corre el comando **"docker build ."**

luego de construido el contenedor se usa el comando **"docker run (ID que se genera al final del build)"**

**¿CÓMO SE SOLUCIONÓ El PROBLEMA?**

La tarea se solucionó a través de la división del problema, donde en vez de trabajar un problema desbalanceado, trato miles de problemas balanceados y través de un sistema de votación selecciono la clasificación final del dato.