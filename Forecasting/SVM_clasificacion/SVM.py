import pandas as pd
import numpy as np
import sklearn as sk
from sklearn.svm import SVC
from sklearn.metrics import f1_score

'leyendo los archivos'
df=pd.read_csv('dataset.csv')
df=df[['x','y','labels']] 

## La idea está en que para balancear las cargas de los elementos de la base de datos, 
## tomo los mismos elementos de ambos conjuntos y hago x cantidad de comparaciones, formando
## un modelo conjunto, donde puedo decir, que si un elemento fue clasificado de un conjunto
## o de otro más de un 80% de las veces, lo puedo tomar como bien clasificado

## Separando los datos que tienen el label 0 y el label 1
X1=df.loc[df['labels']==0]
Y1=df.loc[df['labels']==1]
#print('Y1: ',Y1)
## Divido el set de entrenamiento en 2, 8 elementos de label 1, 8 elementos de label 0

x_train_2=Y1.iloc[0:8] #Elementos del label 1
#print(x_train_2)
## Hago dos pruebas, una con un elemento de label 0 y otra con un elemento de label 1,
## ya que ambos elementos de label 1 dieron bien, quuero ver que no esté tirando todo
## a donde no es

X_test_1=X1.iloc[1000:1100]
Y_test_1=Y1.iloc[8:10]

test_a=pd.concat([X_test_1,Y_test_1])
test=test_a.drop('labels', axis=1)

## Numero de iteraciones que comparo los sets de 8 elementos en 1 y 8 elementos en 0
n=992

## Vectores donde voy a guardar el resultado de las clasificaciones
pred=[]

for i in range(0,n):
    x_train_1=X1.iloc[i:i+8] #agarro 8 elementos consecutivos de la selección de los datos con label 0
    #print("x_test_1: ",x_test_1)
    x_train=pd.concat([x_train_1,x_train_2])#vuelvo ambos elementos un solo dataframe
    #print("x_test: ",x_test)
    X_train=np.array(x_train.drop('labels', axis=1))#quito las etiquetas
    # print("X_train: ",X_train.shape)
    y_train=np.array(x_train['labels'])#tomo sólo las etiquetas
    # print("y_train: ",y_train.shape)
    clf=SVC(kernel='rbf', gamma='scale')
    clf.fit(X_train,y_train)#entreno los datos a las etiquetas
    #clasifico las etiquetas
    pred.append(clf.predict(test))

pred=np.array(pred)
promedio_columnas=pred.mean(axis=0)
# print(pred)
# print(promedio_columnas)

for i in range(len(promedio_columnas)):
    if promedio_columnas[i]>0.8:
        promedio_columnas[i]=1
    else:
        promedio_columnas[i]=0

test_labels=np.array(test_a['labels'])

print('f1_score_macro: ',f1_score(test_labels, promedio_columnas, average='macro'))
print('f1_score_micro: ',f1_score(test_labels, promedio_columnas, average='micro'))
print('f1_score_weighted: ',f1_score(test_labels, promedio_columnas, average='weighted'))  
print('f1_score_none: ',f1_score(test_labels, promedio_columnas, average=None))