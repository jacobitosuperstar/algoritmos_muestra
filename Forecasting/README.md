# Algoritmos_muestra

**para correr el algoritmo en docker**

Se hace un dockerfile con alpine para correr la aplicación en un container de tamaño mínimo. Se usa el ambiente de miniconda debido a la flexibilidad que tiene para instalar y manejar paquetes, ya que no sólo se liminta a python, permite un manejo general de paquetes tanto en R, php, C++, entre otros.

Aunque el tamaño de la imagen nunca es mejor a un de 1 Gb, la facilidad de construcción y manejo valen la pena.

a través de la términal se accede a la carpeta en donde se encuentra el dockerfile y se corre el comando "**docker build .**"

luego de construido el contenedor se usa el comando "**docker run (ID que se genera al final del build)**"

## Forecasting ##

Dos ejemplos de predicción y clasificación usando Support Vector Machines, donde se alcanza un 80% de presición en la función estudiada con 100 datos iniciales y 50 datos de predicción, y un 94% de presición en la clasificación con más de 2mil muestras de una clase y 8 muestras de otra, donde se emplea un sistema interno de votación para seleccionar la clasificación.
Dos ejemplos de predicción y clasificación usando Support Vector Machines, donde se alcanza un 80% de presición en la función estudiada, y un 94% de presición en la clasificación.
