# -*- coding: utf-8 -*-
"""
Created on Thu Oct 25 13:40:43 2018

@author: jacobobedoya
"""
import pandas as pd

#path_consultas_psicologicas="/Users/jacobobedoya/Documents/cosas_del_trabajo/Consultas-psicológicas-2018-08-01-17_04_33-2018-10-20 17_04_33.xlsx"    ##Casa
path_consultas_psicologicas="C:/Users/jacobobedoya/bitbucket/cosas_del_trabajo/Consultas-psicológicas-2018-08-01-17_04_33-2018-10-20 17_04_33.xlsx"   ##Trabajo

consultas_psicologicas=pd.read_excel(path_consultas_psicologicas)

consultas_psicologicas=consultas_psicologicas[["DOCUMENTO",
                                               "ESTUDIANTE",
                                               "PROGRAMA",
                                               "ASESOR SIGA",
                                               "ID ASESOR", 
                                               "TIPO DE ASESOR",
                                               "TTipo de consulta",
                                               "Motivo de consulta"]]

consultas_psicologicas.columns=["Documento",
                                "ESTUDIANTE",
                                "PROGRAMA",
                                "ASESOR SIGA",
                                "ID ASESOR", 
                                "TIPO DE ASESOR",
                                "TTipo de consulta",
                                "Motivo de consulta"]

path_grupos_psicopedagogico="C:/Users/jacobobedoya/bitbucket/cosas_del_trabajo/Grupos psicopedagógicos - 2018-08-01 17_08_33 - 2018-10-20 17_08_33.xlsx" ##Trabajo
#path_grupos_psicopedagogico=
# grupos_psicopedagogico=pd.read_excel(path_grupos_psicopedagogico)
# grupos_psicopedagogico=grupos_psicopedagogico[[""]]

#path_BajoRendimientoUenMiBarrio="/Users/jacobobedoya/Documents/cosas_del_trabajo/BRUenmiBarrio2018-2.xlsx"  ##Casa
path_BajoRendimientoUenMiBarrio="C:/Users/jacobobedoya/bitbucket/cosas_del_trabajo/BRUenmiBarrio2018-2.xlsx" ##Trabajo
BajoRendimientoUenMiBarrio=pd.read_excel(path_BajoRendimientoUenMiBarrio)

#path="/Users/jacobobedoya/Documents/cosas_del_trabajo/EST BR FRATERNIDAD Y ROBLEDO REPARTIDOS 2018-2.xlsx"      ##Casa
path="C:/Users/jacobobedoya/bitbucket/cosas_del_trabajo/EST BR FRATERNIDAD Y ROBLEDO REPARTIDOS 2018-2.xlsx"    ##Trabajo 

MAFE_BR_RF=pd.read_excel(path, sheet_name="MAFE")
MAFE_BR_RF=MAFE_BR_RF[["Documento",
                       "Carnet",
                       "Nombres",
                       "Apellidos",
                       "Programa",
                       "Facultad",
                       "Telefono",
                       "Celular",
                       "Correo Electronico",
                       "Correo Alterno"]]
MAFE_BR_RF=pd.merge(MAFE_BR_RF,
                    consultas_psicologicas,
                    how='left', 
                    on=["Documento"])
MAFE_BR_RF=pd.DataFrame.drop_duplicates(MAFE_BR_RF)
# print("MAFE_BR_RF", MAFE_BR_RF.columns)

SANDRA_BR_RF=pd.read_excel(path, sheet_name="SANDRA")
SANDRA_BR_RF=SANDRA_BR_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico","Correo Alterno"]]
SANDRA_BR_RF=pd.merge(SANDRA_BR_RF,consultas_psicologicas,how='left',on=["Documento"])
SANDRA_BR_RF=pd.DataFrame.drop_duplicates(SANDRA_BR_RF)
# print("SANDRA_BR_RF",SANDRA_BR_RF.columns)

ANDRES_BR_RF=pd.read_excel(path, sheet_name="ANDRES")
ANDRES_BR_RF=ANDRES_BR_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico","Correo Alterno"]]
ANDRES_BR_RF=pd.merge(ANDRES_BR_RF,consultas_psicologicas,how='left', on=["Documento"])
ANDRES_BR_RF=pd.DataFrame.drop_duplicates(ANDRES_BR_RF)
# print("ANDRES_BR_RF", ANDRES_BR_RF.columns)

MARIA_ISABEL_BR_RF=pd.read_excel(path, sheet_name="MA ISABEL")
MARIA_ISABEL_BR_RF=MARIA_ISABEL_BR_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico","Correo Alterno"]]
MARIA_ISABEL_BR_RF=pd.merge(MARIA_ISABEL_BR_RF,consultas_psicologicas,how='left', on=["Documento"])
MARIA_ISABEL_BR_RF=pd.DataFrame.drop_duplicates(MARIA_ISABEL_BR_RF)
# print("MARIA_ISABEL_BR_RF", MARIA_ISABEL_BR_RF.columns)

ALEJO_BR_RF=pd.read_excel(path, sheet_name="ALEJO")
ALEJO_BR_RF=ALEJO_BR_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico","Correo Alterno"]]
ALEJO_BR_RF=pd.merge(ALEJO_BR_RF,consultas_psicologicas,how='left', on=["Documento"])
ALEJO_BR_RF=pd.DataFrame.drop_duplicates(ALEJO_BR_RF)
# print("ALEJO_BR_RF", ALEJO_BR_RF.columns)

FRANCY_BR_RF=pd.read_excel(path, sheet_name="FRANCY")
FRANCY_BR_RF=FRANCY_BR_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico","Correo Alterno"]]
FRANCY_BR_RF=pd.merge(FRANCY_BR_RF,consultas_psicologicas,how='left', on=["Documento"])
FRANCY_BR_RF=pd.DataFrame.drop_duplicates(FRANCY_BR_RF)
# print("FRANCY_BR_RF", FRANCY_BR_RF.columns)

# TOTALES_BR_RF=pd.read_excel(path, sheet_name="TOTALES")

FLORESTA_BR_RF=pd.read_excel(path, sheet_name="FLORESTA")
FLORESTA_BR_RF=FLORESTA_BR_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico","Correo Alterno"]]
FLORESTA_BR_RF=pd.merge(FLORESTA_BR_RF,consultas_psicologicas,how='left', on=["Documento"])
FLORESTA_BR_RF=pd.DataFrame.drop_duplicates(FLORESTA_BR_RF)
# print("FLORESTA_BR_RF", FLORESTA_BR_RF.columns)

CASTILLA_BR_RF=pd.read_excel(path, sheet_name="CASTILLA")
CASTILLA_BR_RF=CASTILLA_BR_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico","Correo Alterno"]]
CASTILLA_BR_RF=pd.merge(CASTILLA_BR_RF,consultas_psicologicas,how='left', on=["Documento"])
CASTILLA_BR_RF=pd.DataFrame.drop_duplicates(CASTILLA_BR_RF)
# print("MARIA_ISABEL_BR_RF", CASTILLA_BR_RF.columns)

CLAUDIA_BR_RF=pd.read_excel(path, sheet_name="CLAUDIA")
CLAUDIA_BR_RF=CLAUDIA_BR_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico","Correo Alterno"]]
CLAUDIA_BR_RF=pd.merge(CLAUDIA_BR_RF,consultas_psicologicas,how='left', on=["Documento"])
CLAUDIA_BR_RF=pd.DataFrame.drop_duplicates(CLAUDIA_BR_RF)
# print("MARIA_ISABEL_BR_RF", CLAUDIA_BR_RF.columns)

DIANA_BR_RF=pd.read_excel(path, sheet_name="DIANA")
DIANA_BR_RF=DIANA_BR_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico","Correo Alterno"]]
DIANA_BR_RF=pd.merge(DIANA_BR_RF,consultas_psicologicas,how='left', on=["Documento"])
DIANA_BR_RF=pd.DataFrame.drop_duplicates(DIANA_BR_RF)
# print("DIANA_BR_RF", DIANA_BR_RF.columns)

RUBEN_BR_RF=pd.read_excel(path, sheet_name="RUBEN")
RUBEN_BR_RF=RUBEN_BR_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico","Correo Alterno"]]
RUBEN_BR_RF=pd.merge(RUBEN_BR_RF,consultas_psicologicas,how='left', on=["Documento"])
RUBEN_BR_RF=pd.DataFrame.drop_duplicates(RUBEN_BR_RF)
# print("RUBEN_BR_RF", RUBEN_BR_RF.columns)

ANDREA_BR_RF=pd.read_excel(path, sheet_name="ANDREA")
ANDREA_BR_RF=ANDREA_BR_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico","Correo Alterno"]]
ANDREA_BR_RF=pd.merge(ANDREA_BR_RF,consultas_psicologicas,how='left', on=["Documento"])
ANDREA_BR_RF=pd.DataFrame.drop_duplicates(ANDREA_BR_RF)
# print("ANDREA_BR_RF", ANDREA_BR_RF.columns)

MONICA_BR_RF=pd.read_excel(path, sheet_name="MONICA")
MONICA_BR_RF=MONICA_BR_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico","Correo Alterno"]]
MONICA_BR_RF=pd.merge(MONICA_BR_RF,consultas_psicologicas,how='left', on=["Documento"])
MONICA_BR_RF=pd.DataFrame.drop_duplicates(MONICA_BR_RF)
# print("MONICA_BR_RF", MONICA_BR_RF.columns)

JUANJO_BR_RF=pd.read_excel(path, sheet_name="JUANJO")
JUANJO_BR_RF=JUANJO_BR_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico","Correo Alterno"]]
JUANJO_BR_RF=pd.merge(JUANJO_BR_RF,consultas_psicologicas,how='left', on=["Documento"])
JUANJO_BR_RF=pd.DataFrame.drop_duplicates(JUANJO_BR_RF)
# print("JUANJO_BR_RF", JUANJO_BR_RF.columns)

FATIMA_BR_RF=pd.read_excel(path, sheet_name="FATIMA")
FATIMA_BR_RF=FATIMA_BR_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico","Correo Alterno"]]
FATIMA_BR_RF=pd.merge(FATIMA_BR_RF,consultas_psicologicas,how='left', on=["Documento"])
FATIMA_BR_RF=pd.DataFrame.drop_duplicates(FATIMA_BR_RF)
# print("FATIMA_BR_RF", FATIMA_BR_RF.columns)

writer=pd.ExcelWriter('EST BR FRATERNIDAD Y ROBLEDO REPARTIDOS 2018-2 atentidos.xlsx')
MAFE_BR_RF.to_excel(writer,sheet_name='MAFE')
SANDRA_BR_RF.to_excel(writer,sheet_name='SANDRA')
ANDRES_BR_RF.to_excel(writer,sheet_name='ANDRES')
MARIA_ISABEL_BR_RF.to_excel(writer,sheet_name='MARIA ISABEL')
ALEJO_BR_RF.to_excel(writer,sheet_name='ALEJO')
FRANCY_BR_RF.to_excel(writer,sheet_name='FRANCY')
# TOTALES_BR_RF.to_excel(writer,sheet_name='TOTALES')
FLORESTA_BR_RF.to_excel(writer,sheet_name='FLORESTA')
CASTILLA_BR_RF.to_excel(writer,sheet_name='CASTILLA')
CLAUDIA_BR_RF.to_excel(writer,sheet_name='CALUDIA')
DIANA_BR_RF.to_excel(writer,sheet_name='DIANA')
RUBEN_BR_RF.to_excel(writer,sheet_name='RUBEN')
ANDREA_BR_RF.to_excel(writer,sheet_name='ANDREA')
MONICA_BR_RF.to_excel(writer,sheet_name='MONICA')
JUANJO_BR_RF.to_excel(writer,sheet_name='JUANJO')
FATIMA_BR_RF.to_excel(writer,sheet_name='FATIMA')
writer.save()

print("1")
#################################################################################################################################################################
path="C:/Users/jacobobedoya/bitbucket/cosas_del_trabajo/EST N S U FRATERNIDAD Y ROBLEDO REPARTIDOS 2018-2.xlsx" ##Trabajo
#path="/Users/jacobobedoya/Documents/cosas_del_trabajo/EST N S U FRATERNIDAD Y ROBLEDO REPARTIDOS 2018-2.xlsx"   ##Casa

SANDRA_NSU_RF=pd.read_excel(path, sheet_name="SANDRA")
SANDRA_NSU_RF=SANDRA_NSU_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico"]]
SANDRA_NSU_RF=pd.merge(SANDRA_NSU_RF,consultas_psicologicas,how='left',on=["Documento"])
SANDRA_NSU_RF=pd.DataFrame.drop_duplicates(SANDRA_NSU_RF)
#print("SANDRA_NSU_RF",SANDRA_NSU_RF.columns)

MAFE_NSU_RF=pd.read_excel(path, sheet_name="MAFE")
MAFE_NSU_RF=MAFE_NSU_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico"]]
MAFE_NSU_RF=pd.merge(MAFE_NSU_RF,consultas_psicologicas,how='left',on=["Documento"])
MAFE_NSU_RF=pd.DataFrame.drop_duplicates(MAFE_NSU_RF)
#print("MAFE_NSU_RF",MAFE_NSU_RF.columns)

ANDRES_NSU_RF=pd.read_excel(path, sheet_name="ANDRES")
ANDRES_NSU_RF=ANDRES_NSU_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico"]]
ANDRES_NSU_RF=pd.merge(ANDRES_NSU_RF,consultas_psicologicas,how='left',on=["Documento"])
ANDRES_NSU_RF=pd.DataFrame.drop_duplicates(ANDRES_NSU_RF)
#print("ANDRES_NSU_RF",ANDRES_NSU_RF.columns)

FRANCY_NSU_RF=pd.read_excel(path, sheet_name="FRANCY")
FRANCY_NSU_RF=FRANCY_NSU_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico"]]
FRANCY_NSU_RF=pd.merge(FRANCY_NSU_RF,consultas_psicologicas,how='left',on=["Documento"])
FRANCY_NSU_RF=pd.DataFrame.drop_duplicates(FRANCY_NSU_RF)
#print("FRANCY_NSU_RF",FRANCY_NSU_RF.columns)

MARIA_ISABEL_NSU_RF=pd.read_excel(path, sheet_name="MARIA ISABEL")
MARIA_ISABEL_NSU_RF=MARIA_ISABEL_NSU_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico"]]
MARIA_ISABEL_NSU_RF=pd.merge(MARIA_ISABEL_NSU_RF,consultas_psicologicas,how='left',on=["Documento"])
MARIA_ISABEL_NSU_RF=pd.DataFrame.drop_duplicates(MARIA_ISABEL_NSU_RF)
#print("MARIA_ISABEL_NSU_RF",MARIA_ISABEL_NSU_RF.columns)

ALEJANDRO_NSU_RF=pd.read_excel(path, sheet_name="ALEJANDRO")
ALEJANDRO_NSU_RF=ALEJANDRO_NSU_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico"]]
ALEJANDRO_NSU_RF=pd.merge(ALEJANDRO_NSU_RF,consultas_psicologicas,how='left',on=["Documento"])
ALEJANDRO_NSU_RF=pd.DataFrame.drop_duplicates(ALEJANDRO_NSU_RF)
#print("ALEJANDRO_NSU_RF",ALEJANDRO_NSU_RF.columns)

JUANJO_NSU_RF=pd.read_excel(path, sheet_name="JUANJO")
JUANJO_NSU_RF=JUANJO_NSU_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico"]]
JUANJO_NSU_RF=pd.merge(JUANJO_NSU_RF,consultas_psicologicas,how='left',on=["Documento"])
JUANJO_NSU_RF=pd.DataFrame.drop_duplicates(JUANJO_NSU_RF)
#print("JUANJO_NSU_RF",JUANJO_NSU_RF.columns)

RUBEN_NSU_RF=pd.read_excel(path, sheet_name="RUBEN")
RUBEN_NSU_RF=RUBEN_NSU_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico"]]
RUBEN_NSU_RF=pd.merge(RUBEN_NSU_RF,consultas_psicologicas,how='left',on=["Documento"])
RUBEN_NSU_RF=pd.DataFrame.drop_duplicates(RUBEN_NSU_RF)
#print("RUBEN_NSU_RF",RUBEN_NSU_RF.columns)

CLAUDIA_NSU_RF=pd.read_excel(path, sheet_name="CLAUDIA")
CLAUDIA_NSU_RF=CLAUDIA_NSU_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico"]]
CLAUDIA_NSU_RF=pd.merge(CLAUDIA_NSU_RF,consultas_psicologicas,how='left',on=["Documento"])
CLAUDIA_NSU_RF=pd.DataFrame.drop_duplicates(CLAUDIA_NSU_RF)
#print("CLAUDIA_NSU_RF",CLAUDIA_NSU_RF.columns)

MONICA_NSU_RF=pd.read_excel(path, sheet_name="MONICA")
MONICA_NSU_RF=MONICA_NSU_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico"]]
MONICA_NSU_RF=pd.merge(MONICA_NSU_RF,consultas_psicologicas,how='left',on=["Documento"])
MONICA_NSU_RF=pd.DataFrame.drop_duplicates(MONICA_NSU_RF)
#print("MONICA_NSU_RF",MONICA_NSU_RF.columns)

FATIMA_NSU_RF=pd.read_excel(path, sheet_name="FATIMA")
FATIMA_NSU_RF=FATIMA_NSU_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico"]]
FATIMA_NSU_RF=pd.merge(FATIMA_NSU_RF,consultas_psicologicas,how='left',on=["Documento"])
FATIMA_NSU_RF=pd.DataFrame.drop_duplicates(FATIMA_NSU_RF)
#print("FATIMA_NSU_RF",FATIMA_NSU_RF.columns)

DIANA_NSU_RF=pd.read_excel(path, sheet_name="DIANA")
DIANA_NSU_RF=DIANA_NSU_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico"]]
DIANA_NSU_RF=pd.merge(DIANA_NSU_RF,consultas_psicologicas,how='left',on=["Documento"])
DIANA_NSU_RF=pd.DataFrame.drop_duplicates(DIANA_NSU_RF)
#print("DIANA_NSU_RF",DIANA_NSU_RF.columns)

ANDREA_NSU_RF=pd.read_excel(path, sheet_name="ANDREA")
ANDREA_NSU_RF=ANDREA_NSU_RF[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico"]]
ANDREA_NSU_RF=pd.merge(ANDREA_NSU_RF,consultas_psicologicas,how='left',on=["Documento"])
ANDREA_NSU_RF=pd.DataFrame.drop_duplicates(ANDREA_NSU_RF)
#print("ANDREA_NSU_RF",ANDREA_NSU_RF.columns)

# TOTALES_NSU_RF=pd.read_excel(path, sheet_name="TOTALES")

path_estudiantes_nivelSerU="C:/Users/jacobobedoya/bitbucket/cosas_del_trabajo/ESTUDIANTES NIVEL SER U FLORESTA Y CASTILLA 2018-2.xlsx" ##trabajo
#path_estudiantes_nivelSerU="/Users/jacobobedoya/Documents/cosas_del_trabajo/ESTUDIANTES NIVEL SER U FLORESTA Y CASTILLA 2018-2.xlsx" ##casa

estudiantes_nivelSerU_castilla=pd.read_excel(path_estudiantes_nivelSerU,sheet_name="Castilla")
estudiantes_nivelSerU_castilla=estudiantes_nivelSerU_castilla[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico"]]
estudiantes_nivelSerU_castilla=pd.merge(estudiantes_nivelSerU_castilla,consultas_psicologicas,how='left',on=["Documento"])
estudiantes_nivelSerU_castilla=pd.DataFrame.drop_duplicates(estudiantes_nivelSerU_castilla)
#print("estudiantes_nivelSerU_castilla",estudiantes_nivelSerU_castilla.columns)

estudiantes_nivelSerU_floresta=pd.read_excel(path_estudiantes_nivelSerU,sheet_name="Floresta")
estudiantes_nivelSerU_floresta=estudiantes_nivelSerU_floresta[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico"]]
estudiantes_nivelSerU_floresta=pd.merge(estudiantes_nivelSerU_floresta,consultas_psicologicas,how='left',on=["Documento"])
estudiantes_nivelSerU_floresta=pd.DataFrame.drop_duplicates(estudiantes_nivelSerU_floresta)
#print("estudiantes_nivelSerU_floresta",estudiantes_nivelSerU_floresta.columns)

estudiantes_nivelSerU_UenmiBarrio=pd.read_excel(path_estudiantes_nivelSerU,sheet_name="U en mi B")
estudiantes_nivelSerU_UenmiBarrio=estudiantes_nivelSerU_UenmiBarrio[["Documento","Carnet","Nombres","Apellidos","Programa","Facultad","Telefono","Celular","Correo Electronico"]]
estudiantes_nivelSerU_UenmiBarrio=pd.merge(estudiantes_nivelSerU_UenmiBarrio,consultas_psicologicas,how='left',on=["Documento"])
estudiantes_nivelSerU_UenmiBarrio=pd.DataFrame.drop_duplicates(estudiantes_nivelSerU_UenmiBarrio)
#print("estudiantes_nivelSerU_UenmiBarrio",estudiantes_nivelSerU_UenmiBarrio.columns)

writer=pd.ExcelWriter('EST NIVEL SER U REPARTIDOS 2018-2 atentidos.xlsx')
SANDRA_NSU_RF.to_excel(writer,sheet_name='SANDRA')
MAFE_NSU_RF.to_excel(writer,sheet_name='MAFE')
ANDRES_NSU_RF.to_excel(writer,sheet_name='ANDRES')
FRANCY_NSU_RF.to_excel(writer,sheet_name='FRANCY')
MARIA_ISABEL_NSU_RF.to_excel(writer,sheet_name='MARIA ISABEL')
ALEJANDRO_NSU_RF.to_excel(writer,sheet_name='ALEJANDRO')
JUANJO_NSU_RF.to_excel(writer,sheet_name='JUANJO')
RUBEN_NSU_RF.to_excel(writer,sheet_name='RUBEN')
CLAUDIA_NSU_RF.to_excel(writer,sheet_name='CLAUDIA')
MONICA_NSU_RF.to_excel(writer,sheet_name='MONICA')
FATIMA_NSU_RF.to_excel(writer,sheet_name='FATIMA')
DIANA_NSU_RF.to_excel(writer,sheet_name='DIANA')
ANDREA_NSU_RF.to_excel(writer,sheet_name='ANDREA')
estudiantes_nivelSerU_castilla.to_excel(writer,sheet_name='CASTILLA')
estudiantes_nivelSerU_floresta.to_excel(writer,sheet_name='FLORESTA')
estudiantes_nivelSerU_UenmiBarrio.to_excel(writer,sheet_name='UenmiBarrio')
writer.save()

print("2")