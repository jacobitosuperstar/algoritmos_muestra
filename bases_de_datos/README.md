* Algoritmos_muestra *

# Bases de datos #
Se incluye un ejemplo de trabajo, donde a cada psicólogo del proyecto de apoyo estudiantil SIGA, se le chequea cuantas de las personas asignadas fueron atendidas hasta el momento y si fueron atendidas por él o por alguien más. Todo con el fin de evaluar el desempeño individual de cada psicólogo y evaluar el cumplimiento de los requisitos del cargo.

Debido al flujo de trabajo, es más eficiente tener Hojas de Excel directamente conectadas al servidor que tener el archivo de python conectado al servidor. Y los resultados son impresos en hojas de Excel para el fácil entendimiento y seguimiento por parte del jefe del proyecto.