# Algoritmos_muestra
En el siguiente commit se incluyen 3 carpetas con el fin de exponer algo del trabajo y la investigación que estoy realizando. Se incluye el docker file para los algoritmos de optimización, para que puedan ser probados desde cualquier PC. Los archivos de excel generador en docker, no son extraibles, por ende se incluyen aparte.

**para correr el algoritmo en docker**

Se hace un dockerfile con alpine para correr la aplicación en un container de tamaño mínimo. Se usa el ambiente de miniconda debido a la flexibilidad que tiene para instalar y manejar paquetes, ya que no sólo se liminta a python, permite un manejo general de paquetes tanto en R, php, C++, entre otros.

Aunque el tamaño de la imagen nunca es mejor a un de 1 Gb, la facilidad de construcción y manejo valen la pena.

a través de la términal se accede a la carpeta en donde se encuentra el dockerfile y se corre el comando "**docker build .**"

luego de construido el contenedor se usa el comando "**docker run (ID que se genera al final del build)**"

## Bases de datos ##
Se incluye un ejemplo de trabajo, donde a cada psicólogo del proyecto de apoyo estudiantil SIGA, se le chequea cuantas de las personas asignadas fueron atendidas hasta el momento y si fueron atendidas por él o por alguien más.

Debido al flujo de trabajo, es más eficiente tener Hojas de Excel directamente conectadas al servidor que tener el archivo de python conectado al servidor. Y los resultados son impresos en hojas de Excel para el fácil entendimiento y seguimiento por parte del jefe del proyecto.

## Optimización ##
Se incluyen algoritmos referentes al Particle Swarm Optimization, el cual es usado en mi trabajo de grado y un ejemplo aplicado, donde utilizo procesamiento en paralelo para hacer mejor cálculo en computadores de bajo rendimiento. El ejemplo aplicado es el lanzamiento de una pelota en tiro parabólico y se prueba la diferencia de ejecución en el algoritmo 100 veces, debido a que es Eurístico.

Se imprime una hoja de Excel donde se documentan todos las simulaciones de lanzamientos y la convergencia de las respuestas que produce el algoritmo.

## Modelos Ocultos de Márkov ##
Se incluye el paquete que estoy diseñando para la realización del trabajo de grado, referente a la predicción del comportamiento del espacio espectral bajo un flujo continuo de observaciones.

## Forecasting ##

Dos ejemplos de predicción y clasificación usando Support Vector Machines, donde se alcanza un 80% de presición en la función estudiada con 100 datos iniciales y 50 datos de predicción, y un 94% de presición en la clasificación con más de 2mil muestras de una clase y 8 muestras de otra, donde se emplea un sistema interno de votación para seleccionar la clasificación.
Dos ejemplos de predicción y clasificación usando Support Vector Machines, donde se alcanza un 80% de presición en la función estudiada, y un 94% de presición en la clasificación.
